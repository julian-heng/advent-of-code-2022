#!/usr/bin/env python3

import sys

from functools import cmp_to_key, partial
from itertools import zip_longest


def main(fname):
    with open(fname) as f:
        l = (i for i in f.read().split("\n\n"))
        l = tuple(tuple(eval(j) for j in i.splitlines()) for i in l)
        l2 = tuple(i for j in l for i in j) + ([[2]], [[6]])
    print(solve1(l))
    print(solve2(l2))


def solve1(l):
    return sum(n + 1 for n, (a, b) in enumerate(l) if cmp(a, b) < 1)


def solve2(l):
    s = sorted(l, key=cmp_to_key(cmp))
    return (s.index([[2]]) + 1) * (s.index([[6]]) + 1)


def cmp(l, r):
    if l is None or r is None:
        return -1 if l is None else 1
    if isinstance(l, int) and isinstance(r, int):
        return -1 if l < r else 0 if l == r else 1
    if isinstance(l, int) != isinstance(r, int):
        l = [l] if isinstance(l, int) else l
        r = [r] if isinstance(r, int) else r
    result = -1
    for a, b in zip_longest(l, r):
        result = cmp(a, b)
        if result != 0:
            return result
    return result


if __name__ == "__main__":
    main(sys.argv[1] if len(sys.argv) > 1 else "input")
