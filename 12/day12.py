#!/usr/bin/env python3

import sys

from heapq import heappop, heappush


def main(fname):
    with open(fname) as f:
        t = {"S": ord("a"), "E": ord("z")}
        l = f.read().splitlines()
        s = next((y, x) for y, r in enumerate(l) for x, c in enumerate(r)
                  if c == "S")
        e = next((y, x) for y, r in enumerate(l) for x, c in enumerate(r)
                  if c == "E")
        a = tuple((y, x) for y, r in enumerate(l) for x, c in enumerate(r)
                  if c == "a")
        l = tuple(tuple(t.get(j, ord(j)) - ord("a") for j in i) for i in l)
    print(solve(l, s, (e,), adj4_front))
    print(solve(l, e, a, adj4_back))


def solve(l, s, e, adj):
    q = [(0, *s)]
    c = {}
    while q:
        d, y1, x1 = heappop(q)
        if (y1, x1) in e:
            break
        for y, x in adj(l, y1, x1):
            if (y, x) not in c or d + 1 < c[y, x]:
                c[y, x] = d + 1
                heappush(q, (d + 1, y, x))
    return d


def adj4_front(l, y, x):
    yield from ((a, b) for a, b, in adj4(l, y, x) if l[a][b] <= l[y][x] + 1)


def adj4_back(l, y, x):
    yield from ((a, b) for a, b, in adj4(l, y, x) if l[a][b] >= l[y][x] - 1)


def adj4(l, y, x):
    if (y + 1) < len(l):
        yield (y + 1, x)
    if (y - 1) >= 0:
        yield (y - 1, x)
    if (x + 1) < len(l[y]):
        yield (y, x + 1)
    if (x - 1) >= 0:
        yield (y, x - 1)


if __name__ == "__main__":
    main(sys.argv[1] if len(sys.argv) > 1 else "input")
