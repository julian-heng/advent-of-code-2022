#!/usr/bin/env python3

import sys


def main(fname):
    with open(fname) as f:
        l = tuple(tuple(int(j) for j in i) for i in f.read().splitlines())
        t = tuple(i for i in zip(*l))
    print(solve1(l, t))
    print(solve2(l, t))


def solve1(l, t):
    v = (c for y, r in enumerate(l)
           for x, c in enumerate(r) if is_visible(l, t, y, x))
    return len(list(v))


def solve2(l, t):
    v = (get_score(c, l, t, y, x) for y, r in enumerate(l)
                                  for x, c in enumerate(r))
    return max(v)


def is_visible(l, t, y, x):
    if y == 0 or x == 0 or y == len(l) - 1 or x == len(l[y]) - 1:
        return True
    return any(all(l[y][x] > j for j in i) for i in rays_4(l, t, y, x))


def get_score(c, l, t, y, x):
    score = 1
    for ray in rays_4(l, t, y, x):
        if ray:
            n = next((n + 1 for n, i in enumerate(ray) if i >= c), len(ray))
            score *= n
        else:
            return 0
    return score


def rays_4(l, t, y, x):
    yield l[y][:x][::-1]
    yield l[y][x+1:]
    yield t[x][:y][::-1]
    yield t[x][y+1:]


if __name__ == "__main__":
    main(sys.argv[1] if len(sys.argv) > 1 else "input")
