#!/usr/bin/env python3

import re
import sys

from collections import deque


def main(fname):
    with open(fname) as f:
        c, l = process_input(f.read())
    print(solve1(tuple(map(deque.copy, c)), l))
    print(solve2(tuple(map(deque.copy, c)), l))


def process_input(s):
    a, b = s.split("\n\n")[:2]
    [h, *a] = a.splitlines()[::-1]
    b = b.splitlines()
    n = max(map(int, h.split()))
    c = tuple(deque() for _ in range(n))
    for i in a:
        for j in range(n):
            if i[(j*4)+1] != ' ':
                c[j].append(i[(j*4)+1])
    l = tuple(tuple(map(int, re.findall(r"\d+", i)[:3])) for i in b)
    return c, l


def solve1(s, l):
    for a, b, c in l:
        for _ in range(a):
            s[c-1].append(s[b-1].pop())
    return "".join(i[-1] for i in s)


def solve2(s, l):
    t = deque()
    for a, b, c in l:
        for _ in range(a):
            t.append(s[b-1].pop())
        for _ in range(len(t)):
            s[c-1].append(t.pop())
    return "".join(i[-1] for i in s)


if __name__ == "__main__":
    main(sys.argv[1] if len(sys.argv) > 1 else "input")
