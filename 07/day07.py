#!/usr/bin/env python3

import sys

from collections import deque


def main(fname):
    with open(fname) as f:
        t = process_input(f.read())
    print(solve1(t))
    print(solve2(t))


def process_input(s):
    cmds = (i.strip() for i in s.split("$"))
    cmds = (i.splitlines() for i in cmds if i)
    cmds = (tuple(tuple((j.split() + ['', ''])[:2]) for j in i) for i in cmds)
    path = deque()
    cwd = "/"
    node = {"/": dict()}
    root = node
    for (cmd, param), *out in cmds:
        if cmd == "cd":
            if param == "..":
                cwd, node = path.pop()
            else:
                path.append((cwd, node))
                node = node[cwd]
                cwd = param
        elif cmd == "ls":
            node[cwd] = {k: dict() if v == "dir" else int(v) for v, k in out}
    return root["/"]


def solve1(t):
    l = []
    lookup_sizes(t, l)
    return sum(i for i in l if i < 100_000)


def solve2(t):
    target = dir_size(t) - 40_000_000
    l = []
    lookup_sizes(t, l)
    return min(i for i in l if i > target)


def lookup_sizes(t, l=[]):
    if not isinstance(t, dict):
        return t
    l.append(dir_size(t))
    for k, v in t.items():
        lookup_sizes(v, l)


def dir_size(t):
    if not isinstance(t, dict):
        return t
    return sum(i for i in (dir_size(v) for k, v in t.items()))


if __name__ == "__main__":
    main(sys.argv[1] if len(sys.argv) > 1 else "input")
