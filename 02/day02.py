#!/usr/bin/env python3

import sys


def main(fname):
    with open(fname) as f:
        l = tuple(tuple(i.split()) for i in f.read().splitlines())
    print(solve1(l))
    print(solve2(l))


def solve1(l):
    t = {"A": 1, "X": 1, "B": 2, "Y": 2, "C": 3, "Z": 3}
    return sum(t[p2] + (6 if winner(p1, p2) == p2 else
                        3 if draw(p1, p2) else
                        0) for p1, p2 in l)


def solve2(l):
    t = {"A": 1, "X": 1, "B": 2, "Y": 2, "C": 3, "Z": 3}
    t2 = {"Z": 6, "Y": 3, "X": 0}
    w = {"A": "Y", "B": "Z", "C": "X"}
    f = {"A": "Z", "B": "X", "C": "Y"}
    d = {"A": "X", "B": "Y", "C": "Z"}
    return sum(t2[p2] + (t[(w if p2 == "Z" else
                            f if p2 == "X" else
                            d)[p1]]) for p1, p2 in l)


def winner(p1, p2):
    r = {"A", "X"}
    p = {"B", "Y"}
    s = {"C", "Z"}
    return p2 if (
        (p2 in r and p1 in s)
        or (p2 in p and p1 in r)
        or (p2 in s and p1 in p)
    ) else p1


def draw(p1, p2):
    r = {"A", "X"}
    p = {"B", "Y"}
    s = {"C", "Z"}
    return (
        (p1 in r and p2 in r)
        or (p1 in p and p2 in p)
        or (p1 in s and p2 in s)
    )


if __name__ == "__main__":
    main(sys.argv[1] if len(sys.argv) > 1 else "input")
