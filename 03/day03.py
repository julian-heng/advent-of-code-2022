#!/usr/bin/env python3

import sys


def main(fname):
    with open(fname) as f:
        l = f.read().splitlines()
        l1 = tuple((set(i[:len(i)//2]), set(i[len(i)//2:])) for i in l)
        l2 = tuple(tuple(map(set, l[i:i+3]) for i in range(0, len(l), 3)))
    print(solve(l1))
    print(solve(l2))


def solve(l):
    return sum(i - (6 if i > 26 else 0)
        for i in (ord(i) - ord('A') + 1
        for i in (i.lower() if i.isupper() else i.upper()
        for i in (next(iter(set.intersection(*i))) for i in l))))


if __name__ == "__main__":
    main(sys.argv[1] if len(sys.argv) > 1 else "input")
