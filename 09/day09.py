#!/usr/bin/env python3

import sys


def main(fname):
    with open(fname) as f:
        l = tuple(tuple(int(j) if j.isnumeric() else j for j in i.split())
                for i in f.read().splitlines())
    print(solve(l, 2))
    print(solve(l, 10))


def solve(l, n):
    v = {(0, 0)}
    p = [(0, 0)] * n
    for (d, m) in l:
        for _ in range(m):
            y, x = p[0]
            x += 1 if d == "R" else -1 if d == "L" else 0
            y += 1 if d == "U" else -1 if d == "D" else 0
            p[0] = (y, x)
            for i in range(1, n):
                _y, _x = p[i-1]
                y, x = p[i]
                if (_y, _x) not in adj(y, x):
                    y += 1 if _y > y else -1 if y != _y else 0
                    x += 1 if _x > x else -1 if x != _x else 0
                if i == n - 1:
                    v.add((y, x))
                p[i] = (y, x)
    return len(v)


def adj(y, x):
    yield y + 1, x - 1
    yield y + 1, x
    yield y + 1, x + 1
    yield y, x - 1
    yield y, x
    yield y, x + 1
    yield y - 1, x - 1
    yield y - 1, x
    yield y - 1, x + 1


if __name__ == "__main__":
    main(sys.argv[1] if len(sys.argv) > 1 else "input")
