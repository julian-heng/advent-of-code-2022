#!/usr/bin/env python3

import re
import sys

from collections import deque
from math import lcm, prod


def main(fname):
    with open(fname) as f:
        l = process_input(f.read())
    print(solve(l, 20, lambda x: x // 3))
    print(solve(l, 10000, lambda x: x))


def process_input(s):
    reg = (
        r"((?:\d+,?\s*)+\d+)(?:[^+-\/*]+)([+-\/*])\s*(\d+|old)"
        r"(?:(?:[^\d]+)(\d+))(?:(?:[^\d]+)(\d+))(?:(?:[^\d]+)(\d+))"
    )
    l = re.findall(reg, s)
    l = tuple(
        (tuple(map(int, a.split(","))), b, int(c) if c.isnumeric() else c
        , *map(int, d)) for a, b, c, *d in l
    )
    return l


def solve(l, n, z):
    mod = lcm(*[i for _, _, _, i, *_ in l])
    m = [deque(i) for i, *_ in l]
    c = {}
    for _ in range(n):
        for n2, (items, (_, op, v, test, t, f)) in enumerate(zip(m, l)):
            if op == "*":
                calc = (lambda x: x * x) if v == "old" else lambda x: x * v
            elif op == "+":
                calc = (lambda x: x + x) if v == "old" else lambda x: x + v
            count = c.get(n2, 0)
            while items:
                count += 1
                b = items.popleft() % mod
                b = z(calc(b))
                m[t if b % test == 0 else f].append(b)
            c[n2] = count
    return prod(sorted(c.values())[-2:])


if __name__ == "__main__":
    main(sys.argv[1] if len(sys.argv) > 1 else "input")
