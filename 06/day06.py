#!/usr/bin/env python3

import sys


def main(fname):
    with open(fname) as f:
        s = f.read()
    print(solve(s, 4))
    print(solve(s, 14))


def solve(s, n):
    return next(i + n for i in range(0, len(s) - n) if len(set(s[i:i+n])) == n)


if __name__ == "__main__":
    main(sys.argv[1] if len(sys.argv) > 1 else "input")
