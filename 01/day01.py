#!/usr/bin/env python3

import sys


def main(fname):
    with open(fname) as f:
        l = f.read().split("\n\n")
        l = tuple(tuple(int(j) for j in i.splitlines()) for i in l)
    print(solve1(l))
    print(solve2(l))


def solve1(l):
    return max(map(sum, l))


def solve2(l):
    return sum(sorted(map(sum, l))[-3:])


if __name__ == "__main__":
    main(sys.argv[1] if len(sys.argv) > 1 else "input")
