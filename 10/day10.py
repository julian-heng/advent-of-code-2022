#!/usr/bin/env python3

import sys


def main(fname):
    with open(fname) as f:
        l = tuple((a, int(b)) for a, b in ((i.split() + [0])[:2]
                              for i in f.read().splitlines()))
    print(solve1(l))
    print(solve2(l))


def solve1(l):
    s = []
    def step(c, x):
        c += 1
        s.append(c * x if (c - 20) % 40 == 0 else 0)
        return c
    _solve(l, step)
    return sum(s)


def solve2(l):
    g = [["." for _ in range(40)] for _ in range(6)]
    s = 0
    r = 0
    def step(c, x):
        nonlocal r
        g[r][c%40] = "#" if c % 40 in range(x-1, x+2) else g[r][c%40]
        c += 1
        r += 1 if c % 40 == 0 else 0
        return c
    _solve(l, step)
    return "\n".join("".join(i) for i in g)


def _solve(l, step):
    c = 0
    x = 1
    for op, v in l:
        if op == "noop":
            c = step(c, x)
        elif op == "addx":
            c = step(c, x)
            c = step(c, x)
            x += v


if __name__ == "__main__":
    main(sys.argv[1] if len(sys.argv) > 1 else "input")
