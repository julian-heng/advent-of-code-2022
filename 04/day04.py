#!/usr/bin/env python3

import sys


def main(fname):
    with open(fname) as f:
        l = (i.split(",") for i in f.read().splitlines())
        l = tuple(tuple(tuple(map(int, j.split("-"))) for j in i) for i in l)
    print(solve1(l))
    print(solve2(l))


def solve1(l):
    return len(list((a, b) for a, b in l if (
        (a[0] <= b[0] and a[-1] >= b[-1]) or (b[0] <= a[0] and b[-1] >= a[-1])
    )))


def solve2(l):
    return len(list((a, b) for a, b in l if (
        (b[0] <= a[-1] <= b[-1]) or (a[0] <= b[-1] <= a[-1])
    )))


if __name__ == "__main__":
    main(sys.argv[1] if len(sys.argv) > 1 else "input")
